import {Component} from '@angular/core';
import * as go from 'gojs';

@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css'],
})
export class AppComponent {
	title = 'GoJSOrgChart';
	public model: go.TreeModel = new go.TreeModel([
		{key: 1, name: 'Horse', img: 'assets/images/horse1.jpg'},
		{key: 2, name: 'Horse', parent: 1, img: 'assets/images/horse2.jpg'},
		{key: 3, name: 'Horse', parent: 2, img: 'assets/images/horse3.jpg'},
		{key: 4, name: 'Horse', parent: 1, img: 'assets/images/horse4.jpg'},
		{key: 5, name: 'Horse', parent: 4, img: 'assets/images/horse5.jpg'},
		{key: 6, name: 'Horse', parent: 2, img: 'assets/images/horse6.jpg'},
		{key: 7, name: 'Horse', parent: 3, img: 'assets/images/horse7.jpg'},
		{key: 8, name: 'Horse', parent: 5, img: 'assets/images/horse1.jpg'},
		{key: 9, name: 'Horse', parent: 6, img: 'assets/images/horse7.jpg'},
		{key: 10, name: 'Horse', parent: 4, img: 'assets/images/horse6.jpg'},
		{key: 11, name: 'Horse', parent: 6, img: 'assets/images/horse4.jpg'},
		{key: 12, name: 'Horse', parent: 10, img: 'assets/images/horse3.jpg'},
		{key: 13, name: 'Horse', parent: 10, img: 'assets/images/horse2.jpg'},
	]);
	public setSelectedNode(node) {
		console.log(node.data.name);
		console.log(node);
	}
}

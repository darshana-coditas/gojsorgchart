import {AfterViewInit, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import * as go from 'gojs';

const $ = go.GraphObject.make;
@Component({
	selector: 'app-diagram',
	templateUrl: './diagram.component.html',
	styleUrls: ['./diagram.component.css'],
})
export class DiagramComponent implements OnInit, AfterViewInit {
	@Input() model: go.Model;
	@Output() nodeClicked = new EventEmitter();
	public diagram: go.Diagram = null;

	constructor() {}
	ngAfterViewInit(): void {
		this.diagram = $(go.Diagram, 'myDiagramDiv', {
			initialContentAlignment: go.Spot.Left,
			initialAutoScale: go.Diagram.Uniform,
			// isReadOnly: true, // do not allow users to modify or select in this view
			// allowSelect: false,
			layout: $(go.TreeLayout, {
				isOngoing: true,
				treeStyle: go.TreeLayout.StyleLastParents,
				arrangement: go.TreeLayout.ArrangementHorizontal,
				// properties for most of the tree:
				angle: 0,
				layerSpacing: 80,
				// properties for the "last parents":
				alternateAngle: 0,
				// alignment:
				alternateLayerSpacing: 80,
				alternateAlignment: go.TreeLayout.AlignmentCenterSubtrees,
				alternateNodeSpacing: 20,
			}),
			'undoManager.isEnabled': true,
		});

		this.diagram.nodeTemplate = $(
			go.Node,
			'Auto',
			// for sorting, have the Node.text be the data.name
			new go.Binding('text', 'name'),
			// bind the Part.layerName to control the Node's layer depending on whether it isSelected
			new go.Binding('layerName', 'isSelected', function (sel) {
				return sel ? 'Foreground' : '';
			}).ofObject(),
			// define the node's outer shape
			$(
				go.Shape,
				'Rectangle',
				{
					name: 'SHAPE',
					fill: 'lightblue',
					stroke: null,
					figure: 'RoundedRectangle',
					// set the port properties:
					portId: '',
					fromLinkable: false,
					toLinkable: false,
					cursor: 'pointer',
				},
				new go.Binding('fill', '', function (node) {
					// modify the fill based on the tree depth level
					const levelColors = ['#E3F0F1'];
					let color = node.findObject('SHAPE').fill;
					const dia: go.Diagram = node.diagram;
					if (dia && dia.layout.network) {
						dia.layout.network.vertexes.each(function (v: go.TreeVertex) {
							if (v.node && v.node.key === node.data.key) {
								const level: number = v.level % levelColors.length;
								color = levelColors[level];
							}
						});
					}
					return color;
				}).ofObject()
			),
			$(
				go.Panel,
				'Horizontal',
				$(
					go.Picture,
					{
						name: 'Picture',
						desiredSize: new go.Size(44, 50),
						margin: new go.Margin(6, 8, 6, 10),
						// border:new go.,
					},
					new go.Binding('source', 'img').makeTwoWay()
				),
				// define the panel where the text will appear
				$(
					go.Panel,
					'Table',
					{
						maxSize: new go.Size(150, 999),
						margin: new go.Margin(6, 10, 0, 3),
						defaultAlignment: go.Spot.Left,
					},
					$(go.RowColumnDefinition, {column: 2, width: 4}),
					$(
						go.TextBlock,
						{font: '9pt  Segoe UI,sans-serif', stroke: '#354051'},
						{
							row: 0,
							column: 0,
							columnSpan: 5,
							font: '12pt Segoe UI,sans-serif',
							editable: true,
							isMultiline: false,
							minSize: new go.Size(10, 16),
						},
						new go.Binding('text', 'name').makeTwoWay()
					)
				)
			)
		);

		this.diagram.linkTemplate = $(
			go.Link,
			{routing: go.Link.Orthogonal, corner: 5},
			$(go.Shape, {strokeWidth: 3, stroke: '#555'})
		);
		this.diagram.model = this.model;
		this.diagram.addDiagramListener('ChangedSelection', (e) => {
			const node = this.diagram.selection.first();
			this.nodeClicked.emit(node);
		});
	}

	ngOnInit() {}
}
